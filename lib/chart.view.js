(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("backbone.marionette"), require("amcharts/amcharts"), require("amcharts/lang/ru.js"), require("amcharts/serial"), require("amcharts/pie"));
	else if(typeof define === 'function' && define.amd)
		define(["backbone.marionette", "amcharts/amcharts", "amcharts/lang/ru.js", "amcharts/serial", "amcharts/pie"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("backbone.marionette"), require("amcharts/amcharts"), require("amcharts/lang/ru.js"), require("amcharts/serial"), require("amcharts/pie")) : factory(root["backbone.marionette"], root["amcharts/amcharts"], root["amcharts/lang/ru.js"], root["amcharts/serial"], root["amcharts/pie"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_10__, __WEBPACK_EXTERNAL_MODULE_11__, __WEBPACK_EXTERNAL_MODULE_14__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(1);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BasicChartView = _backbone2.default.LayoutView.extend({
    model: false,
    regions: {
        filter: '.chart-switch__header',
        chart: '.chart-switch__body'
    },
    disableFilter: false,
    initialize: function initialize() {
        if (this.getOption('chartView')) {
            this.chartView = this.getOption('chartView');
        }

        if (this.getOption('filterView')) {
            this.filterView = this.getOption('filterView');
        }

        if (this.getOption('model')) {
            this.model = this.getOption('model');
        }
    },
    filterView: false,
    chartView: false,
    template: _.template('<div class="chart-switch__header"></div><div class="chart-switch__body"></div>'),
    onShow: function onShow() {
        this._showFilter();
        this._showChart();
    },
    _showFilter: function _showFilter() {
        if (!this.disableFilter) {
            this.showChildView('filter', new this.filterView({
                model: this.model
            }));
        }
    },
    _showChart: function _showChart() {
        this.showChildView('chart', new this.chartView({
            model: this.model,
            amchartOptions: this.getOption('amchartOptions')
        }));
    }
}); // BasicChartView
// ---------
exports.default = BasicChartView;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("backbone.marionette");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(1);

var _backbone2 = _interopRequireDefault(_backbone);

__webpack_require__(8);

__webpack_require__(9);

__webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// CommonChart
// ---------

var CommonChart = _backbone2.default.ItemView.extend({
    model: false,
    modelEvents: {
        'change': '_changed'
    },
    amchart: false,
    amchartOptions: {
        autoResize: true,
        theme: 'default',
        language: 'ru',
        dataProvider: [],
        listeners: false
    },
    template: false,
    initialize: function initialize() {
        this.amchartOptions.listeners = [{
            'event': 'rendered',
            'method': _.bind(this._chartRendered, this)
        }];
    },
    onAttach: function onAttach() {
        this.model.fetch();
    },
    onBeforeDestroy: function onBeforeDestroy() {
        if (this.amchart) {
            this.amchart.clearLabels();
            this.amchart.removeLegend();
            this.amchart.clear();
            this.amchart = false;
        }
    },
    _changed: function _changed() {
        var that = this;

        if (this.amchart) {
            this.amchart.dataProvider = this.model.getPreparedResult();
            this.amchart.validateData();
        } else {
            var options = _.result(this, 'amchartOptions');
            _.extend(options, this.getOption('amchartOptions'));

            options.dataProvider = this.model.getPreparedResult();
            this.amchart = window.AmCharts.makeChart(that.$el.get(0), options);
        }
    },
    _chartRendered: function _chartRendered(event) {
        this.trigger('chart:rendered', event);
    }
});

exports.default = CommonChart;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(1);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CommonFilter = _backbone2.default.LayoutView.extend({
    model: false,
    initialize: function initialize() {},
    template: false,
    onShow: function onShow() {
        var that = this;
    }
}); // CommonFilter
// ---------

exports.default = CommonFilter;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _version = __webpack_require__(5);

var _basicChartView = __webpack_require__(0);

var _basicChartView2 = _interopRequireDefault(_basicChartView);

var _serialChartView = __webpack_require__(6);

var _serialChartView2 = _interopRequireDefault(_serialChartView);

var _pieChartView = __webpack_require__(12);

var _pieChartView2 = _interopRequireDefault(_pieChartView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// CharView
// ---------
var ChartView = {
    version: _version.version,
    BasicChartView: _basicChartView2.default,
    SerialChartView: _serialChartView2.default,
    PieChartView: _pieChartView2.default
};

exports.default = ChartView;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = {"version":"0.0.2","buildDate":"2017-11-10"}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _basicChartView = __webpack_require__(0);

var _basicChartView2 = _interopRequireDefault(_basicChartView);

var _serialChart = __webpack_require__(7);

var _serialChart2 = _interopRequireDefault(_serialChart);

var _commonFilter = __webpack_require__(3);

var _commonFilter2 = _interopRequireDefault(_commonFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SerialChartView = _basicChartView2.default.extend({
    model: false,
    filterView: _commonFilter2.default,
    chartView: _serialChart2.default
}); // SerialChartView
// ---------

exports.default = SerialChartView;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _commonChart = __webpack_require__(2);

var _commonChart2 = _interopRequireDefault(_commonChart);

__webpack_require__(11);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// SerialChart
// ---------

var SerialChart = _commonChart2.default.extend({
    amchartOptions: function amchartOptions() {
        return _.extend({}, _commonChart2.default.prototype.amchartOptions, {
            type: 'serial',
            graphs: [{
                valueField: 'value'
            }],
            valueAxes: [{
                axisAlpha: 0,
                inside: true,
                dashLength: 3
            }],
            chartCursor: {
                valueLineEnabled: true,
                categoryBalloonDateFormat: 'DD MMM YYYY'
            },
            dataDateFormat: 'YYYY-MM-DD HH:NN:SS',
            categoryField: 'date',
            categoryAxis: {
                parseDates: true,
                minPeriod: 'DD',
                dateFormats: [{
                    period: 'fff',
                    format: 'HH:NN:SS'
                }, {
                    period: 'ss',
                    format: 'HH:NN:SS'
                }, {
                    period: 'mm',
                    format: 'HH:NN'
                }, {
                    period: 'hh',
                    format: 'HH:NN'
                }, {
                    period: 'DD',
                    format: 'DD MMM'
                }, {
                    period: 'WW',
                    format: 'DD MMM'
                }, {
                    period: 'MM',
                    format: 'MMM YYYY'
                }, {
                    period: 'YYYY',
                    format: 'MMM YYYY'
                }]
            }
        });
    }
});

exports.default = SerialChart;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("amcharts/amcharts");

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = AmCharts.themes.default = {

    themeName: 'default',

    AmChart: {
        fontFamily: 'Open Sans',
        fontSize: 14,
        color: '#909eac',
        backgroundColor: '#ffffff',
        pathToImages: '/img/amcharts/'
    },

    AmCoordinateChart: {
        colors: ['#ff6700', '#fdd400', '#84b761', '#cc4748', '#cd82ad', '#2f4074', '#448e4d', '#b7b83f', '#b9783f', '#b93e3d', '#913167']
    },

    AmSlicedChart: {
        colors: ['#dfb645', '#68ae0d', '#67b7dc', '#cc4748', '#cd82ad', '#2f4074', '#448e4d', '#b7b83f', '#b9783f', '#b93e3d', '#913167'],
        backgroundColor: '#f6f9fb',
        outlineAlpha: 0,
        outlineThickness: 0,
        labelTickColor: '#2a2a2a',
        labelTickAlpha: 0.3
    },

    AmGraph: {
        fillAlphas: 0.1,
        lineThickness: 3,
        bullet: 'square',
        bulletAlpha: 0,
        bulletSize: 1,
        bulletBorderColor: '#ffffff',
        bulletBorderAlpha: 0,
        bulletBorderThickness: 0
    },

    AxisBase: {
        axisColor: '#ebeff2',
        axisThickness: 1
    },

    CategoryAxis: {
        gridColor: '#cad4df',
        gridAlpha: 1,
        gridThickness: 1,
        gridPosition: 'start',
        dashLength: 4
    },

    ChartCursor: {
        cursorColor: '#ff6700',
        valueLineEnabled: true,
        color: '#ffffff',
        cursorAlpha: 0.3
    }
};

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("amcharts/lang/ru.js");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("amcharts/serial");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _basicChartView = __webpack_require__(0);

var _basicChartView2 = _interopRequireDefault(_basicChartView);

var _pieChart = __webpack_require__(13);

var _pieChart2 = _interopRequireDefault(_pieChart);

var _commonFilter = __webpack_require__(3);

var _commonFilter2 = _interopRequireDefault(_commonFilter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PieChartView = _basicChartView2.default.extend({
    model: false,
    filterView: _commonFilter2.default,
    chartView: _pieChart2.default
}); // PieChartView
// ---------

exports.default = PieChartView;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _commonChart = __webpack_require__(2);

var _commonChart2 = _interopRequireDefault(_commonChart);

__webpack_require__(14);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// PieChart
// ---------

var PieChart = _commonChart2.default.extend({
    amchartOptions: function amchartOptions() {
        return _.extend({}, _commonChart2.default.prototype.amchartOptions, {
            type: 'pie',

            categoryField: 'date',

            /* Make the chart stretch to full width/height of the container */
            autoMargins: false,
            labelsEnabled: false,
            marginTop: 0,
            marginBottom: 0,
            marginLeft: 0,
            marginRight: 0,
            pullOutRadius: 0,

            radius: '50%',
            innerRadius: '50%'
        });
    }

});

exports.default = PieChart;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("amcharts/pie");

/***/ })
/******/ ]);
});
//# sourceMappingURL=chart.view.js.map