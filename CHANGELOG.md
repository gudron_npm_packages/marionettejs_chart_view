# Changelog

## [Unreleased]
- ColumnChart
- BarChart
- Migration to Marionettejs v3

## [v0.0.2] - 2017-11-10
### Added
- Export wrapper
- Changelog file

### Changed
- One entrypoint to chartview

## [v0.0.1] - 2017-10-05
### Added
-  Project init